CC=gcc
SOURCES=$(wildcard *.c) ./kiss_fft130/tools/kiss_fftr.c ./kiss_fft130/kiss_fft.c
OBJ=$(SOURCES:.c=.o)

CFLAGS=--std=c99 -I./kiss_fft130 -I./kiss_fft130/tools
LDFLAGS=-lasound -lm
EXE=audio_grabber

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(EXE) $(OBJ) $(LDFLAGS)

%.o: %.cpp 
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm *.o $(EXE)

