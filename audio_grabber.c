// Some code from http://equalarea.com/paul/alsa-audio.html 
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <alsa/asoundlib.h>

#include "kiss_fftr.h"
#define N_SEMITONES 12
#define N_SCALEDEGREES 7
#define N_HARMONICS 5 
#define N_SAMPLES 1024 
#define FFT_SIZE 1024 
#define SAMPLE_RATE 44100
#define M_PI 3.14159265358979323846
#define M3 1.25f
#define m3 1.2f
#define M2 1.125f
#define P4 1.3333f
#define P5 1.5f
#define M6 1.6667f
#define M7 1.875f
#define A2 110.0f
#define SCALING_FACTOR 200
#define MAG_THRESHOLD 30000
#define FREQ_LOWER_THRESHOLD 80
#define MAJOR 0
#define MINOR 1


/*
 * Below is a series of flags used for the validation mask, sharp mask and flat mask.
 */  

short sdOne = 1;
short sdTwo = 2;
short sdThree = 4;
short sdFour = 8;
short sdFive = 16;
short sdSix = 32;
short sdSeven = 64; 

/*
 * Note: the number of fragments required is finicky. For whatever reason
 * the math I use to calculate frames is off by a power of two. Probably
 * just oversight.
 *
 *
 */


char *snd_device_in = "default";
char *snd_device_out = "default";
snd_pcm_t *capture_handle;
snd_pcm_t *playback_handle;
int nchannels = 1;
int bits = 16;
int restarting;
int buffer_size = N_SAMPLES;
int fragments = 4;
long overlap = 4; //overlap rate
static float bpm = 160.0f;
static float f0 = 110.0f; //Root frequency, expected to change, default A2
const static float semitones[12] = { 1.0f, 16.0f/15.0f, 9.0f/8.0f, 6.0f/5.0f, 5.0f/4.0f, 4.0f/3.0f, 64.0f/45.0f, 3.0f/2.0f, 8.0f/5.0f, 5.0f/3.0f, 16.0f/9.0f, 15.0f/8.0f }; //Tuning system we're using 
int majorScale[7] = { 0, 2, 4, 5, 7, 9, 11 }; //Remember this is 0 indexed!!!
int minorScale[7] = { 0, 2, 3, 5, 7, 8, 10 };
int harmonicThreshold = 3; // Generated notes are at least 3 semitones away, can be modified for later
int currentScale[7] = { 0, 2, 4, 5, 7, 9, 11};

/*
 * This is the struct definition that will be used to process the musical notation input.
 * Has time data as well as a permutation of a ratio matrix based off the semitones array.
 * Is a portion of a list with pointer to next chord incoming.
 */

typedef struct ToneBlock {

	short chordMembers; //Mask for validation
	int relativeScale [7];	
	int root;
	float duration;
	struct ToneBlock* next;

} ToneBlock;


/*
 * Input root is 1 indexed, stored root is 0 indexed. 
 */

ToneBlock* createToneBlock(int root, float beats, short flatmask, short sharpmask, short valmask){
	
	int zeroroot= root - 1;
	ToneBlock* block = (ToneBlock*)malloc(sizeof(ToneBlock));

	//Find chords scale in terms of scale degrees/ratios
	for(int i = zeroroot, n = 0; i < 7; i++, n++){
		block->relativeScale[n] = currentScale[i];
	}

	for(int i = 0, n = (7 - zeroroot); i < zeroroot; i++, n++){
		block->relativeScale[n] = currentScale[i];

	}
/*	for(int i = 0; i < 7; i++){

		fprintf(stdout, "root %d degree %d current %d relative %d \n", root, i, currentScale[i], block->relativeScale[i]);

	}
*/	block->duration = beats;
	block->chordMembers = valmask;
	block->root = zeroroot;
	//Apply sharp and flat masks (they should cancel eachother out if theyre both 0xFF
	for(int i = 0; i < 7; i++){
		short mask = 1<<i;
		if(flatmask&mask){
			block->relativeScale[i] = block->relativeScale[i] - 1;
		}

		if(sharpmask&mask){
			block->relativeScale[i] = block->relativeScale[i] + 1;
		}

	}

	return block;
} 



float getClosestRatio(int givenSemitone, int threshold, int lowFlag, int printer){
	
	int targetSemitone = -1;
	int closestinterval = 50; //min for lower, max for upper
	int closestscaledegree = -1;
	int currinterval;

	for(int yui = 0; yui < N_SCALEDEGREES ; yui++){
		currinterval = currentScale[yui] - givenSemitone;
		if(printer){
			//fprintf(stdout, "interval calculation curr %d, scale degree %d,  semitone %d, givenSemitone %d", currinterval, yui, currentScale[yui], givenSemitone);
		}
		if(lowFlag){
			//Lower tone calculation
			currinterval *= -1;
			currinterval = (currinterval + N_SEMITONES)%N_SEMITONES;
			if(currinterval >= threshold && currinterval < closestinterval){
				closestscaledegree = yui;
				closestinterval = currinterval;
			}

		}
		else{
			//upper tone calculation
			currinterval = (currinterval + N_SEMITONES)%N_SEMITONES;
			if(printer){
				fprintf(stdout, "threshold %d scaledegree %d, currinterval %d \n", threshold, yui, currinterval);

			}
			if(currinterval >= threshold && currinterval < closestinterval){
				closestscaledegree = yui;
				closestinterval = currinterval;
			}
		}
	}

if(closestscaledegree > -1){

	if(lowFlag){
		return 1/semitones[closestinterval];
	}	
	return semitones[closestinterval];

}

return 1.0f;

}

float getClosestRatioRef(int givenSemitone, int threshold, int lowFlag, ToneBlock * reference, int printer){
	
	int targetSemitone = -1;
	int closestinterval = 50; //min for lower, max for upper
	int closestscaledegree = -1;
	int currinterval;

	if(printer){
		fprintf(stdout, "control %d \n", reference->chordMembers);

	}
	for(int yui = 0; yui < N_SCALEDEGREES ; yui++){
		if(printer){
			fprintf(stdout, "mask %d, result %d  \n", 1<<yui, (reference->chordMembers & 1<<yui) );
			
		}
		if(!(reference->chordMembers & 1<<yui)){
			continue;
		}
			if(printer){
		fprintf(stdout, "given semitone %d check semitone %d \n", givenSemitone, reference->relativeScale[yui] );
		}
		currinterval = reference->relativeScale[yui] - givenSemitone;
		if(lowFlag){
		/*	if(printer){
				fprintf(stdout, "intervalic subtraction %d \n", currinterval);
			}		//Lower tone calculation
	*/		currinterval *= -1;
		/*	if(printer){
				fprintf(stdout, "intervalic negation %d \n", currinterval);
			}*/
			currinterval = (currinterval + N_SEMITONES)%N_SEMITONES;
			if(printer){
				fprintf(stdout, "intervalic calculation %d \n", currinterval);
			}	
		if(currinterval >=  threshold && currinterval < closestinterval){
			closestscaledegree = yui;
			closestinterval = currinterval;
		}

	}
	else{
		//upper tone calculation
		currinterval = (currinterval + N_SEMITONES)%N_SEMITONES;
		if(printer){
			fprintf(stdout, "threshold %d scaledegree %d, currinterval %d \n", threshold, yui, currinterval);

		}
		if(currinterval >= threshold && currinterval < closestinterval){
			closestscaledegree = yui;
			closestinterval = currinterval;
		}
	}
}

if(printer){
fprintf(stdout, "target %d, interval %d, scaledegree %d \n", targetSemitone, closestinterval, closestscaledegree);

}
if(closestscaledegree > -1){

if(lowFlag){
	return 1/semitones[closestinterval];
}	
return semitones[closestinterval];

}

return 1.0f;

}




float getRatio(int givenSemitone, int targetScaleDegree, ToneBlock * reference){
int targetSemitone = reference->relativeScale[targetScaleDegree];
	int ratioIndex = ((targetSemitone - givenSemitone) + 11)%11;	
	return semitones[ratioIndex];
}


	int
configure_alsa_audio(snd_pcm_t *device, int channels)
{
	snd_pcm_hw_params_t *hw_params;
	int                 err;
	int                 tmp;
	snd_pcm_uframes_t   frames;
	int sample_rate = SAMPLE_RATE;
	int frame_size;

	/* allocate memory for hardware parameter structure */ 
	if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0) {
		fprintf (stderr, "cannot allocate parameter structure (%s)\n",
				snd_strerror(err));
		return 1;
	}
	/* fill structure from current audio parameters */
	if ((err = snd_pcm_hw_params_any(device, hw_params)) < 0) {
		fprintf (stderr, "cannot initialize parameter structure (%s)\n",
				snd_strerror(err));
		return 1;
	}

	/* set access type, sample rate, sample format, channels */
	if ((err = snd_pcm_hw_params_set_access(device, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
		fprintf (stderr, "cannot set access type: %s\n",
				snd_strerror(err));
		return 1;
	}
	// bits = 16
	if ((err = snd_pcm_hw_params_set_format(device, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) {
		fprintf (stderr, "cannot set sample format: %s\n",
				snd_strerror(err));
		return 1;
	}
	tmp = sample_rate;    
	if ((err = snd_pcm_hw_params_set_rate_near(device, hw_params, &tmp, 0)) < 0) {
		fprintf (stderr, "cannot set sample rate: %s\n",
				snd_strerror(err));
		return 1;
	}
	if (tmp != sample_rate) {
		fprintf(stderr, "Could not set requested sample rate, asked for %d got %d\n", sample_rate, tmp);
		sample_rate = tmp;
	}
	if ((err = snd_pcm_hw_params_set_channels(device, hw_params, channels)) < 0) {
		fprintf (stderr, "cannot set channel count: %s\n",
				snd_strerror(err));
		return 1;
	}

	if ((err = snd_pcm_hw_params_set_periods_near(device, hw_params, &fragments, 0)) < 0) {
		fprintf(stderr, "Error setting # fragments to %d: %s\n", fragments,
				snd_strerror(err));
		return 1;
	}

	frame_size = channels * (bits / 8);
	frames = buffer_size / frame_size * fragments;
	if ((err = snd_pcm_hw_params_set_buffer_size_near(device, hw_params, &frames)) < 0) {
		fprintf(stderr, "Error setting buffer_size %d frames: %s\n", frames,
				snd_strerror(err));
		return 1;
	}
	if (buffer_size != frames * frame_size / fragments) {
		fprintf(stderr, "Could not set requested buffer size, asked for %d got %d\n", buffer_size, frames * frame_size / fragments);
		buffer_size = frames * frame_size / fragments;
	}

	if ((err = snd_pcm_hw_params(device, hw_params)) < 0) {
		fprintf(stderr, "Error setting HW params: %s\n",
				snd_strerror(err));
		return 1;
	}
	return 0;
}


float rad_to_Hz(float idx) 
{
	float rad = idx / (float)(N_SAMPLES/4);
	return SAMPLE_RATE * rad;
}

int main (int argc, char *argv[])
{
	int i;
	int c;
	int err;
	short buf[N_SAMPLES];
	short buf_out[N_SAMPLES];
	unsigned int rate = SAMPLE_RATE;
	snd_pcm_hw_params_t *hw_params;
	restarting = 1;
	char *mfilepath;
	FILE *mfile;
	int sflag = 0, mflag = 0, pflag = 0, uflag = 0, iflag = 0, bflag = 0, fflag = 0, lflag = 0, wflag = 0;


	/*
 	* Control Block
 	*/ 

	while ((c = getopt(argc,argv,":swlmpui:b:f:")) != -1) {
		switch(c) {

		case 'w':
			wflag++; //Undertone switch
			break;
		case 'l':
			lflag++; //Leadin flag
			break;
		case 's':
			sflag++; //Synthesis flag
			break;
		case 'm':
			mflag++; //Met flag
			break;
		case 'p':
			pflag++; //Pedal flag
			break;
		case 'u':
			uflag++; //Pedal sUstain flag
			break;
		case 'i':
			iflag++; //Infile flag
			mfilepath = optarg;
			break;
		case 'b':
			bflag++; //bpm set flag
			bpm = atoi(optarg);		
			break;
		case 'f':
			fflag++; //freq set flag
			f0 = atoi(optarg);
			break;
		case ':':
			fprintf(stderr, "Option -%c requires an operand\n", optopt);
			exit(1);
			break;
		case '?':
			fprintf(stderr, "Unrecognized option: '-%c'\n", optopt);
			exit(1);
			break;

		}

	}
		ToneBlock *head;
		ToneBlock *mid;
		ToneBlock *tail;		

	// Read infile
	if(iflag){
		mfile = fopen(mfilepath, "r");
		if(mfile == NULL) {
			fprintf(stderr, "Can't open input file.\n");
			exit(1);
		}

		float nbeats;
		int scaleroot;
		short flats, sharps, valid;
		float tf0;
		int tbpm;
		int mode;
		
		int what = fscanf(mfile, "%f:%d:%d", &tf0, &mode, &tbpm);
		if(!fflag){
			f0 = tf0;
		}

		if(!bflag){
			bpm = tbpm;
		}

		
		switch(mode){
			case MAJOR:
				for(int s = 0; s < 7; s++){
					currentScale[s] = majorScale[s];
				}
				break;
			case MINOR:
				for(int s = 0; s < 7; s++){
					currentScale[s] = minorScale[s];
				}
				break;
			default:
				for(int s = 0; s < 7; s++){
					currentScale[s] = majorScale[s];
				}
				break;
		}	
		int check;	
		int nblocks = 0;
		while( check = fscanf(mfile, "%f;%i;%hx;%hx;%hx", &nbeats, &scaleroot, &flats, &sharps, &valid) == 5 ){
			tail = createToneBlock(scaleroot,nbeats,flats,sharps,valid);
			if(nblocks == 0){
				head = tail;
				mid = tail;
			} 
			else{
				mid->next = tail;
				mid = tail;
			}

			nblocks++;
		}		
		fclose(mfile);
	}
	
	// Open Devices

	if ((err = snd_pcm_open (&playback_handle, snd_device_out, SND_PCM_STREAM_PLAYBACK, 0)) < 0) {
		fprintf (stderr, "cannot open audio device %s (%s)\n", 
				argv[1],
				snd_strerror (err));
		exit (1);
	}

	if ((err = snd_pcm_open (&capture_handle, snd_device_in, SND_PCM_STREAM_CAPTURE, 0)) < 0) {
		fprintf (stderr, "cannot open audio device %s (%s)\n", 
				argv[1],
				snd_strerror (err));
		exit (1);
	}


	configure_alsa_audio(playback_handle, nchannels);
	configure_alsa_audio(capture_handle, nchannels);

	int nr_samples = N_SAMPLES/fragments;  //Size of processing chunk
	int fft_samples = nr_samples * overlap; //FFT uses sample history to get a finer grain resolution

	//Prepare FFT buffers

	kiss_fftr_cfg icfg = kiss_fftr_alloc(fft_samples, 1, NULL ,NULL);
	kiss_fftr_cfg cfg = kiss_fftr_alloc(fft_samples, 0, NULL, NULL);

	// Even though the fft function halves the buffer size, it seems that it doesnt
	// affect the frequency grainularity: check later

	static float cpx_buf[FFT_SIZE];
	static float cpx_buf_out[FFT_SIZE];
	static float trueFrequencyin[FFT_SIZE];
	static int   trueFrequencycount[FFT_SIZE]; // For finding dominant bins.
	static float trueMagnitudein[FFT_SIZE];
	static float lastPhase[FFT_SIZE/2 + 1];  //Might have to halve this again
	static float sumPhase[FFT_SIZE/2 + 1];
	static float trueFrequencyout[FFT_SIZE];
	static float trueMagnitudeout[FFT_SIZE];
	static float outputAccum[FFT_SIZE*2];	
	static int harmonics[N_HARMONICS];

	static float tempPhases[FFT_SIZE];


	float tempPhase = 0.0f;
	float interval = 1.0f;
	float ratio = 0.0f;	
	kiss_fft_cpx cpx_next[fft_samples/2 + 1];
	kiss_fft_cpx cpx_out[fft_samples/2 + 1];
	double magn, phase, tmp, window, real, imag;
	double freqPerBin, expct;
	long qpd, stepSize, latency, index;


	memset(cpx_buf, 0, FFT_SIZE*sizeof(float));
	memset(cpx_buf_out, 0, FFT_SIZE*sizeof(float));	
	memset(trueFrequencyin, 0, (FFT_SIZE)*sizeof(float));
	memset(trueMagnitudein, 0, (FFT_SIZE)*sizeof(float));	
	memset(lastPhase, 0, (FFT_SIZE/2 + 1)*sizeof(float));
	memset(sumPhase, 0, (FFT_SIZE/2 + 1)*sizeof(float));	
	memset(outputAccum, 0, (FFT_SIZE*2)*sizeof(float));
	memset(tempPhases, 0, (FFT_SIZE)*sizeof(float));
	memset(trueFrequencycount, 0, FFT_SIZE*sizeof(int));


	stepSize = nr_samples;
	freqPerBin = (double)SAMPLE_RATE/(double)fft_samples;
	expct = 2.*M_PI*(double)stepSize/(double)fft_samples;
	latency = fft_samples - stepSize;	
	fprintf(stdout, "Samples %d stepsize %d latency %d \n", fft_samples, stepSize, latency);

	float metphase = 0.0f;
	float susphase = 0.0f;
	int frames, inframes, outframes, frame_size;
	frame_size = nchannels * (bits / 8 );
	frames = buffer_size / (frame_size * ((float)fragments/2));
	int ipb = 1/((bpm/60.0f)*((float)frames/(float)SAMPLE_RATE)); 
	int iterate = 0;
	double beats = 0.0;	

	//*******************Playback loop****************************************
	while(iterate > -1){
		iterate++;

		if(restarting){
			restarting = 0;
			snd_pcm_drop(capture_handle);
			snd_pcm_drop(playback_handle);

			snd_pcm_prepare(capture_handle);
			snd_pcm_prepare(playback_handle);

			for (int d = 0; d < fragments; d += 1){
				snd_pcm_writei(playback_handle, buf, frames);
			} 	

		}	
		while ((inframes = snd_pcm_readi (capture_handle, buf, frames)) < 0) {
			if (inframes == -EAGAIN)
				continue;

			fprintf (stderr, "Input buffer overrun\n");

			restarting = 1;
			snd_pcm_prepare(capture_handle);

		}

		if(inframes != frames)
			fprintf(stderr, "Short read from capture device: %d, expecting %d\n", inframes, frames);




		for (int j = 0; j < inframes; ++j) {
			//Print Check	
			//			fprintf(stdout, "iteration %d input: %f \n", iterate, (float)buf[j]);			  
			window = -.5*cos(2.*M_PI*(double)j/(double)inframes)+.5;
			//window = 1;	
			cpx_buf[j + latency] = (float)buf[j]*window;  //Start on the right end of the buffer


		}
		//clear new buffer
		for (int n = 0; n < fft_samples/2 + 1; n++) {
			cpx_next[n].r = 0.0f;
			cpx_next[n].i = 0.0f;
		}




		kiss_fftr(cfg, cpx_buf, cpx_out);

		//Print Check
		/*for(int n = 0; n < nr_samples/2; n++){
		//fprintf(stdout, "Result of fft -- Real: %f Imaginary: %f \n", cpx_out[n].r, cpx_out[n].i);
		}*/	

		//Initialize Variables for max amplitude
		float mx = 0.0f;
		int idx = 0;
		float maxphase = 0.0f;
		float expectedPhase = 0.0f;
		float binFreq = 0.0f;

		cpx_next[0].r = cpx_out[0].r;  //DC constant

		//Harmonization loop TODO: Remove Constants

		memset(harmonics, -1, N_HARMONICS*sizeof(int));	



		for (int j = 1; j < fft_samples/2 + 1; j++) {


			magn = sqrt(cpx_out[j].r*cpx_out[j].r + cpx_out[j].i*cpx_out[j].i);
			phase = atan2(cpx_out[j].i, cpx_out[j].r);

			tmp = phase - lastPhase[j];
			lastPhase[j] = phase;

			tmp -= (double)j*expct;

			qpd = tmp/M_PI;
			if (qpd >= 0) qpd += qpd&1;
			else qpd -= qpd&1;
			tmp -= M_PI*(double)qpd;

			tmp = overlap*tmp/(2.*M_PI);

			tmp = (double)j*freqPerBin + tmp*freqPerBin;

			for(int h = 0; h < N_HARMONICS; h++){


				if(harmonics[h] < 0){
					harmonics[h] = j;
					break;
				}
				else if(magn > trueMagnitudein[harmonics[h]]  && 0 < tmp < 22050 ){

					for(int w = N_HARMONICS - 1; w > h; w--){
						harmonics[w] = harmonics[w - 1];
					}
					harmonics[h] = j;
					break;		 
					//	expectedPhase = (double)j*expct;
					//	binFreq = (double)j*freqPerBin;
				}

			}
			idx = harmonics[0];
			trueFrequencyin[j] = tmp;
			trueMagnitudein[j] = magn;

			//Harmony Calculation
			//cpx_next[(int)(j*2)-1].r += cpx_out[j].r/2;
			//	cpx_next[j].r = cpx_out[j].r;
			//	cpx_next[j].i = cpx_out[j].i;
			//	cpx_next[(int)(j*2)+1].r += cpx_out[j].r/2;
		}
		
		/*if(iterate == ipb){
			for(int w = 0; w < N_HARMONICS; w++){
				fprintf(stdout, "Harmonic %d Freq %f Mag %f \n", w, trueFrequencyin[harmonics[w]], trueMagnitudein[harmonics[w]]);
			}

		}*/

		//fprintf(stdout, "Max Bin %d ExpectedPhase %f Phase %f BinFreq %f TrueFreq %f Mag %f \n", idx, expectedPhase, maxphase, binFreq, trueFrequencyin[idx], trueMagnitudein[idx]);

		//fprintf(stdout, " Frequency: %d Hz index: %d", trueFrequencyin[idx], idx);
		// Before we calculate frequency ratios, we first have to compare proper values.
		double integral = 0.0;
		double initialratio = (trueFrequencyin[idx]/f0);
		initialratio = log2(initialratio);
		float f0t = f0;
/*
 * Pitch detection segment
 */ 
		if(initialratio < 0){
			initialratio = initialratio*-1.0;
			initialratio = modf(initialratio,&integral);
			f0t = f0t/pow(2,(integral + 1));
		
		}
		else if(initialratio >= 1){
			initialratio = modf(initialratio,&integral);
			f0t = f0t*pow(2,integral);


		}

		ratio = trueFrequencyin[idx]/f0t;
		float tfreq = trueFrequencyin[idx];
		/*
 		*
 		* This is where we decide what interval to use
 		*/

		int givenTone;	
		
		for(int st = 0; st < N_SEMITONES; st++){
			if(st == 0){
				if(ratio < (1.0f + semitones[1])/2 || ratio > (semitones[N_SEMITONES - 1] + 2.0f)/2){
							givenTone = 0;
							break;
				}
			}
			
			if( ratio < (semitones[st] + semitones[st + 1])/2 ){
				givenTone = st;
				break;
			}

			if(st == 11){
				givenTone = st;
			}
		}
		
		interval = 1.0f;	
		if(iflag){
			int k = 0;
		/*	if(iterate == ipb){
				k = 1;
			}
		*/	interval = getClosestRatioRef(givenTone, harmonicThreshold, wflag, head, k);

		}
		else{
			int k = 0;
	/*		if(iterate == ipb){
				k = 1;
			}
	*/		interval = getClosestRatio(givenTone, harmonicThreshold, wflag, k);


		}

		if(iterate == ipb){
			fprintf(stdout, "Given freq %f given ratio %f Given tone %d result ratio %f lflag %d \n", tfreq, ratio, givenTone, interval, lflag);	

		}
		if(!sflag){
			//Alter the signal
			memset(trueFrequencyout, 0, FFT_SIZE*sizeof(float));
			memset(trueMagnitudeout, 0, FFT_SIZE*sizeof(float));
			for(int k = 1; k < fft_samples/2 + 1; k++){
				index = k*interval;
				if( index < fft_samples/2 + 1){
					/*
					   Mode Switch
					   */

					if( trueFrequencyin[k] < trueFrequencyin[harmonics[0]]*4/5){

						trueMagnitudeout[index] = 0;
					}
					else{		
						
						trueMagnitudeout[index] += trueMagnitudein[k];
					}
				trueFrequencyout[index] = trueFrequencyin[k] * interval;
				}

			}
/*
			for(int k = 0; k < N_HARMONICS; k++){
				if(!wflag){
				trueMagnitudeout[harmonics[k]] *= 2.0f;
				}
			}*/
			/*for(int nharms = 0; nharms < N_HARMONICS; nharms++ ){ 
			  fprintf(stdout, "Harmonic %d Bin %d Freq %f  mag %f \n", nharms, harmonics[nharms], trueFrequencyin[harmonics[nharms]], trueMagnitudein[harmonics[nharms]]);
			  }			*/	
			for(int k = 0; k < fft_samples/2 + 1; k++){
				magn = trueMagnitudeout[k];
				tmp = trueFrequencyout[k];

				tmp -= (double)k*freqPerBin;

				tmp /= freqPerBin;

				tmp = 2.*M_PI*tmp/(double)overlap;

				tmp += (double)k*expct;

				sumPhase[k] += tmp;
				phase = sumPhase[k];

				cpx_next[k].r = magn*cos(phase);
				cpx_next[k].i = magn*sin(phase);

			}
			//Clarify the signal

			kiss_fftri( icfg, cpx_next, cpx_buf_out);


			for( int indexu = 0; indexu < fft_samples; indexu++){
				window = -.5*cos(2.*M_PI*(double)indexu/(double)fft_samples)+.5;
				outputAccum[indexu] += 2.*window*cpx_buf_out[indexu]/((fft_samples/2)*overlap);
			}

			//Print Check
			//take synthesis result and write first chunk to out
			for (int j = 0; j < nr_samples; ++j) {
				buf[j] = (short)(outputAccum[j]);
				//fprintf(stdout, "iteration %d fftcheck out: %f \n", iterate, (float)cpx_buf_out[j]);		
				/*if( i == 250 ){
				//	 fprintf(stdout, "O: %f \n", cpx_buf[j]/nr_samples);	
				}*/
			}

			memmove(outputAccum, outputAccum+stepSize, fft_samples*sizeof(float));
		}
		else{

			/*
			 * Generated Synthesis Step, Write directly to buffer.	
			 *	 
			 *	 */
			for(int bufi = 0; bufi < inframes; bufi++){
				buf[bufi] = 0;

			}	
			//			fprintf(stdout, "Iteration: %d \n", iterate);
			//			

			for(int nharms = 0; nharms < nr_samples/2; nharms++ ){ 
				//float tempPhase = 0.0f;
				float tempMag = trueMagnitudein[nharms];
				float tempFreq = trueFrequencyin[nharms]*interval;
				trueFrequencycount[nharms]++;	

				//				print check harmonics.

				//fprintf(stdout, "Harmonic %d Bin %d Freq %f  mag %f \n", nharms, harmonics[nharms], tempFreq, tempMag);
				//fprintf(stdout, "generating %d harmonic, with %f mag and %f freq \n", harmonics[nharms], tempMag, tempFreq);

				//Simple Filtering
				//
				if(tempMag > MAG_THRESHOLD && tempFreq > FREQ_LOWER_THRESHOLD){
					for(int bufi = 0; bufi < nr_samples; bufi++){
						window = -.5*cos(2.*M_PI*(double)bufi/(double)fft_samples)+.5;
						buf[bufi] += 2.*window*(tempMag*sin(tempPhases[nharms]))/((fft_samples/2)*overlap);
						//buf[bufi] += (short) (SCALING_FACTOR/10)*log10(tempMag)*sin(tempPhases[nharms]);
						//buf[bufi] += 2000*sin(tempPhase);	
						tempPhases[nharms] = tempPhases[nharms] + (2*M_PI*tempFreq)/(SAMPLE_RATE/2);
						if(tempPhases[nharms] > 2*M_PI){
							tempPhases[nharms] = tempPhases[nharms] - 2*M_PI;
						}

					}				
				}	
			}


			//Synthesized multiple harmonic sound
			/*	
				for(int bufi = 0; bufi < nr_samples; bufi++){
				buf[bufi] += (short) SCALING_FACTOR*log10(600000)*sin(tempPhases[0]);

				buf[bufi] += (short) SCALING_FACTOR*log10(600000)*sin(tempPhases[1]);

				buf[bufi] += (short) SCALING_FACTOR*log10(400000)*sin(tempPhases[2]);

				buf[bufi] += (short) SCALING_FACTOR*log10(400000)*sin(tempPhases[3]);

				buf[bufi] += (short) SCALING_FACTOR*log10(200000)*sin(tempPhases[4]);
				buf[bufi] += (short) SCALING_FACTOR*log10(200000)*sin(tempPhases[5]);

				tempPhases[0] = tempPhases[0] + (2*M_PI*200)/(SAMPLE_RATE/2);
				tempPhases[1] = tempPhases[1] + (2*M_PI*200)/(SAMPLE_RATE/2);
				tempPhases[2] = tempPhases[2] + (2*M_PI*400)/(SAMPLE_RATE/2);
				tempPhases[3] = tempPhases[3] + (2*M_PI*400)/(SAMPLE_RATE/2);
				tempPhases[4] = tempPhases[4] + (2*M_PI*600)/(SAMPLE_RATE/2);
				tempPhases[5] = tempPhases[5] + (2*M_PI*500)/(SAMPLE_RATE/2);

				}
				*/
		}

		/*		for(int p = 0; p < nr_samples; p++){	
				fprintf(stdout, "iteration %d output: %f \n", iterate, (float)buf[p]);			  
				}
				*/				
		if(iterate < 20 && mflag > 0)  {
			for(int d = 0; d < inframes; d++){
				buf[d] += (short) SCALING_FACTOR*5.5*sin(metphase);
				metphase = metphase + (2*M_PI*1000)/(SAMPLE_RATE/2);

			}	

		}

		if(!iflag&&uflag){
			for(int d = 0; d <inframes; d++){
				buf[d] += (short) SCALING_FACTOR*10*sin(susphase);
				susphase = susphase + (2*M_PI*(f0)/(SAMPLE_RATE/2));

			}

		}
		if(iflag&&uflag&&!lflag){
			for(int d = 0; d < inframes; d++){
				buf[d] += (short) SCALING_FACTOR*10*sin(susphase);
				susphase = susphase + (2*M_PI*(semitones[head->relativeScale[0]]*f0)/(SAMPLE_RATE/2));

			}	

		}

		//Shift steps
		for (int k = 0; k < latency; k++){
			cpx_buf[k] = cpx_buf[k + stepSize];
		}
		//fprintf(stdout, "\b X");

		//	printf("0.0\t%d\n",idx);

		while ((outframes = snd_pcm_writei (playback_handle, buf, inframes)) < 0) {
			if (outframes == -EAGAIN)
				continue;

			//	fprintf (stderr, "Output buffer underrun inframes %d outframes %d \n", inframes, outframes);

			restarting = 1;
			snd_pcm_prepare(playback_handle);

		}
		if(outframes != frames)
			fprintf(stderr, "Short write to playback device: %d, expection %d\n", outframes, frames);	
		if(iflag||lflag){
			beats += 1/(double)ipb;
		}
		if(iterate == ipb){
			iterate = 0;
		
		}

		if(lflag){
				if(beats >= 8){
					lflag = 0;
					beats = 0;
					
				}
				continue;
			}
		if(iflag&&!lflag){
			if(beats >= head->duration){
					beats = 0;
					ToneBlock * prev = head;
					susphase = 0;
					free(prev);	
					head = head->next;
					if(head == NULL){
						exit(0);
					}
}
			
		}
		
	
		
	}
	/*
	   for(int count = 0; count < FFT_SIZE ; count ++){
	   fprintf(stdout, "Bin %d count %d \n", count, trueFrequencycount[count]);

	   }
	   */
	free(cfg);
	snd_pcm_close (capture_handle);
	snd_pcm_close (playback_handle);
	return 0;
}
