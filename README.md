This is the repository used for the development of the audio Harmonizer.

Creator: Akira Torrijos

Use the Makefile to Compile the executable.
Flags:
-f set frequency
-b set bpm
-i specify music file
-u generate reference pitch
-w synthesize lower note
-m enable metronome
-l have 8 count lead in
